Image for the MPD-Sima Client
=============================

The [MPD-Sima client](https://kaliko.me/mpd-sima/) queues similar songs
into your [MPD server](https://github.com/MusicPlayerDaemon/MPD) by querying
the Last.fm song database.

Apparently it has a fairly decent starting configuration, so if you just
start it, it works out of the box.

This is a simple Debian based image to run an MPD-Sima client. It uses the
following environment variables:

  - `MPD_HOST`: The MPD to connect to, defaults to `localhost`
  - `MPD_PORT`: On which port to connect to MPD, defaults to `6600`
  - `VAR_DIR`: Where MPD-Sima should store its local data, e.g. cache.
    Defaults to `/var/mpdsima`.

Generally I'm starting this using the provided `run.sh` script, which does
essentially runs an MPD-Sima client into the local network
namespace (`--net=host`), keeping logs on the stdout (`-ti`), with a handy
name (`--name mpdsima`):
```
    $ podman run -ti \
                 --net=host \
                 --name mpdsima \
             registry.gitlab.com/codedump2/mpdsima-image:latest
```

Note that the actual contents of the `run.sh` script may actually deviate
for this; both the script and the description here are just for orientation.
You may want to make a system service out of it:
```
    $ podman run ... -name mpdsima gitlab.com/codedump2/mpdsima-image:latest 
    $ podman generate systemd mpdsima > .config/systemd/user/mpdsima.service
    $ systemd --user daemon-reload
    $ systemd --user enable mpdsima
    $ systemd --user start mpdsima
```
