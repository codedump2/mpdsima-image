FROM debian:11
RUN apt-get update && \
    apt-get -y --no-install-recommends install mpd-sima && \
    mkdir /var/mpdsima

ENV MPD_HOST=localhost
ENV MPD_PORT=6600
ENV VAR_DIR=/var/mpdsima

CMD mpd-sima --host $MPD_HOST --port $MPD_PORT --var-dir $VAR_DIR
